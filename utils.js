"use strict";

var U = module.exports;

// because not all file systems like '*' in a name (and they're scary)
U._tameWild = function tameWild(pathname, wild) {
    if (!wild) {
        return pathname;
    }
    var tame = wild.replace(/\*/g, "_");
    return pathname.replace(wild, tame);
};

U._tpl = function tpl(store, opts, str) {
    var server = ["directoryUrl", "serverDir", "server"];
    var env = ["env", "directoryUrl"];
    [
        ["basePath", "configDir"],
        server,
        ["subject", "hostname", "domain"],
        env
    ].forEach(function(group) {
        group.forEach(function(tmpl) {
            group.forEach(function(key) {
                var item = opts[key] || store.options[key];
                if ("string" !== typeof item) {
                    return;
                }

                if ("directoryUrl" === key) {
                    item = item.replace(/^https?:\/\//i, "");
                }
                if ("env" === tmpl) {
                    if (/staging/.test(item)) {
                        item = "staging";
                    } else if (/acme-v02/.test(item)) {
                        item = "live";
                    } else {
                        // item = item;
                    }
                }

                if (-1 === str.indexOf(":" + tmpl)) {
                    return;
                }
                str = str.replace(":" + tmpl, item);
            });
        });
    });
    return str;
};
