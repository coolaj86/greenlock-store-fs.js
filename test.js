"use strict";

var tester = require("greenlock-store-test");

var crypto = require("crypto");
var os = require("os");
var path = require("path");
var basedir = path.join(
    os.tmpdir(),
    "greenlock-store-fs-test-" + crypto.randomBytes(4).toString("hex")
);
var domain = "*.example.com";
var store = require("./").create({
    configDir: basedir,
    accountsDir: path.join(basedir, "accounts"),
    privkeyPath: path.join(basedir, "live", domain, "privkey.pem"),
    fullchainPath: path.join(basedir, "live", domain, "fullchain.pem"),
    certPath: path.join(basedir, "live", domain, "cert.pem"),
    chainPath: path.join(basedir, "live", domain, "chain.pem"),
    bundlePath: path.join(basedir, "live", domain, "bundle.pem")
});
console.info("Test Dir:", basedir);

tester
    .test(store)
    .then(function() {
        console.info("PASS");
    })
    .catch(function(err) {
        console.error("FAIL");
        console.error(err);
        process.exit(20);
    });
