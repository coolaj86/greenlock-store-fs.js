"use strict";

function getPromise() {
    var util = require("util");
    var PromiseA;
    if (util.promisify && global.Promise) {
        PromiseA = global.Promise;
        PromiseA.promisify = util.promisify;
    } else {
        try {
            PromiseA = require("bluebird");
        } catch (e) {
            console.error(
                "Your version of node is missing Promise. Please run `npm install --save bluebird` in your project to fix"
            );
            process.exit(10);
        }
    }
    return PromiseA;
}

module.exports = getPromise();
